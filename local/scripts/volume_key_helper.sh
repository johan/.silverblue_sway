#!/bin/sh

case $1 in
up)
    /usr/bin/amixer set Master 5%+ | grep "\[" | cut -d[ -f2 | cut -d% -f1 | head -n1
    ;;

down)
    /usr/bin/amixer set Master 5%- | grep "\[" | cut -d[ -f2 | cut -d% -f1 | head -n1
    ;;

*)
    /usr/bin/amixer set Master toggle | grep "Front Left:" | cut -d" " -f 7,8 | tr -d '[]%' | sed 's/[0-9]\+\soff/0/g;s/\son//g'
    



    ;;
esac

CURVOL=`amixer get Master | awk '/Mono.+/ {print $6=="[off]"?$6:$4}' | tr -d '[]'`
echo "$CURVOL"
