# Sway und Silverblue

## Notes

Install these packages.  
```rpm-ostree install bemenu foot grim imv light mako neovim slurp sway swaybg swayidle swaylock wl-clipboard wob zsh```  
```flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo```   
```flatpak install flathub io.gitlab.librewolf-community```   



Copy these files.  
```cp ~/.silverblue_sway/system/40-disable_wakeup_xhc1.rules /etc/udev/rules.d/```   
```cp ~/.silverblue_sway/system/disable_gpe4E.service /etc/systemd/system/```

